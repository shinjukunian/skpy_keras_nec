import TimeSliceImport
import numpy as np
from sklearn import (neural_network)

path="/Users/morten/Desktop/SKYP_KYP_SYP/"
start=201
end=1600
points=1000
types=["yeast"]
spectra=TimeSliceImport.importData(path,types)



trainingSamples, validationSamples = TimeSliceImport.splitData(spectra, 0.25)
trainingSpectraList = list(map(lambda x: x.interpolatedSpectra(start, end, points, removeBackground=True), trainingSamples))

# validationSpectraList = list(map(lambda x: x.interpolatedSpectra(start, end, points, filter="BOTTOMSDEV"), validationSamples))
# trainingData,trainingClasses,validationData,validationClasses = TimeSliceImport.makeMultiDimensionalData(trainingSpectraList, validationSpectraList)


# clf = neural_network.MLPClassifier(solver='lbfgs', alpha=1e-5,hidden_layer_sizes=(20,), random_state=1)
# clf.fit(trainingData,trainingClasses)
# score=clf.score(validationData,validationClasses)
# print("Score %f" % score)
