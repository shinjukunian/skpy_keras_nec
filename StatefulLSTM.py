import keras
from keras.models import Sequential
from keras.layers import Dense, Dropout, Activation, LSTM
from keras.optimizers import SGD
import TimeSliceImport
import numpy as np
from sklearn import preprocessing, metrics


def trainModel(model, trainingSamples,numClasses,epochs=10, start=201, end=1600, points=1000, verbose=True):
    meanLoss = []
    meanAccuracy = []

    for epoch in range(epochs):
        mean_tr_acc = []
        mean_tr_loss = []

        for sample in trainingSamples:
            trainingSpectra, classes = sample.interpolatedSpectra(start, end, points, filter="none")

            oneHotClasses = keras.utils.to_categorical(classes, num_classes=numClasses)

            preprocessing.minmax_scale(trainingSpectra, axis=0, copy=False)

            for i in range(trainingSpectra.shape[1]):
                spectrum = trainingSpectra[:, i]
                tSpect = np.expand_dims(np.expand_dims(spectrum, axis=0), axis=0)

                currentClass = oneHotClasses[i, :]
                currentClassExpanded = np.expand_dims(currentClass, axis=0)

                loss, acc = model.train_on_batch(tSpect, currentClassExpanded)
                mean_tr_acc.append(acc)
                mean_tr_loss.append(loss)

            model.reset_states()

        mA = np.mean(mean_tr_acc)
        mL = np.mean(mean_tr_loss)
        meanLoss.append(mL)
        meanAccuracy.append(mA)

        if verbose:
            print('accuracy training = {}'.format(mA))
            print('loss training = {}'.format(np.mean(mL)))


    return meanLoss, meanAccuracy

def testModel(model, validationSamples,numClasses, start=201, end=1600, points=1000, verbose=True):
    mean_tr_acc = []
    mean_tr_loss = []


    for sample in validationSamples:
        validationSpectra, classes = sample.interpolatedSpectra(start, end, points, filter="none")
        oneHotClasses = keras.utils.to_categorical(classes, num_classes=numClasses)

        preprocessing.minmax_scale(validationSpectra, axis=0, copy=False)

        for i in range(validationSpectra.shape[1]):
            spectrum = validationSpectra[:, i]
            tSpect = np.expand_dims(np.expand_dims(spectrum, axis=0), axis=0)

            currentClass = oneHotClasses[i, :]
            currentClassExpanded = np.expand_dims(currentClass, axis=0)

            loss, acc = model.test_on_batch(tSpect, currentClassExpanded)
            mean_tr_acc.append(acc)
            mean_tr_loss.append(loss)

        model.reset_states()

    mA = np.mean(mean_tr_acc)
    mL = np.mean(mean_tr_loss)

    if verbose:
        print('accuracy test = {}'.format(mA))
        print('loss test = {}'.format(np.mean(mL)))

    return mA,mL


def classifyData(model, samples,start=201, end=1600, points=1000, verbose=True):

    results=[]
    truths=[]
    perTraceResults=[]
    perTraceTruths=[]

    for sample in samples:
        subResult=[]
        validationSpectra, classes = sample.interpolatedSpectra(start, end, points, filter="none")
        sampleClass=np.unique(classes)
        truths.extend(sampleClass)
        perTraceTruths.append(classes.flatten())

        preprocessing.minmax_scale(validationSpectra, axis=0, copy=False)

        for i in range(validationSpectra.shape[1]):
            spectrum = validationSpectra[:, i]
            tSpect = np.expand_dims(np.expand_dims(spectrum, axis=0), axis=0)

            currentClass = classes[i]
            currentClassExpanded = np.expand_dims(currentClass, axis=0)

            res = model.model.predict_on_batch(tSpect)
            classResult=np.argmax(res,axis=1)
            subResult.extend(classResult)

        model.reset_states()

        perTraceResults.append(subResult)
        resultClasses, counts =np.unique(subResult,return_counts=True)
        resultClass=resultClasses[np.argmax(counts)]
        results.append(resultClass)


    score=metrics.accuracy_score(truths,results)

    if verbose:
        print("Score: {}".format(score))

    return truths,results,perTraceTruths,perTraceResults
