import os
from scipy import interpolate
from scipy.signal import savgol_filter
import numpy as np
from sklearn import (preprocessing)
import random
import math
from functools import reduce
import xml.etree.ElementTree as ET
import codecs
from collections import namedtuple


Augmentation=namedtuple("Augmentation",["fold","deltaX","deltaYNoise","deltaYMultiplierNoise","combine"])


class RamanSample:
    mappings = {"kleb": 1, "spore": 0, "PS": 2, "yeast": 3}
    filetypes=[".txt",".s1d"]

    def __init__(self, filePath):
        self.name = os.path.basename(filePath)
        for (key) in self.mappings.keys():
            if key in self.name:
                self.typeName = key
                self.type = self.mappings[key]

        extension = os.path.splitext(filePath)[1]
        if extension == ".txt":
            self.readFile(filePath)
        elif extension == ".s1d":
            self.readXML(filePath)
        else:
            print("Unknown filetype for {}".format(filePath))

    def readFile(self, filePath):
        with open(filePath, "r") as file:
            lines = file.read().splitlines()
            wn = []
            spectra = []
            for idx, line in enumerate(lines):
                components = line.split("\t")
                if idx == 0:
                    times = list(map(lambda v: float(v), components[1:]))
                    self.times = np.asarray(times)
                else:
                    wn.append(float(components[0]))
                    spectra.append(list(map(lambda v: float(v), components[1:])))
        self.spectra = np.asarray(spectra)
        self.waveNumbers = np.asarray(wn)

    def prettyName(self):
        n = "_".join(str(self.name).split(".")[0].split("_")[2:])
        return n

    def readXML(self,filePath):#only support for time scans so far
        endTag="</SPECTRADATA>"
        xmlText=""
        dataoffset=0
        with codecs.open(filePath, "r", encoding='utf-8', errors='ignore') as file:
            line=""
            while endTag not in line:
                line=file.readline()
                xmlText += line


        root=ET.fromstring(xmlText)

        # frameoptions=root.find("./ScannedFrameParameters/FrameOptions")
        # wL=float(frameoptions.find("OmuLaserWLnm").text)

        dataDimensions = root.find("./ScannedFrameParameters/DataCalibration/DataDimentions/Channel0")
        channelSize=int(dataDimensions.find("ChannelSize").text)
        excitation=float(dataDimensions.find("ChannelAxisLaserWl").text)
        wavelengths_Strings=dataDimensions.find("ChannelAxisArray").text.split(" ")
        wavenumbers=[]

        for wLs in wavelengths_Strings:
            if len(wLs) >0:
                wL=float(wLs)
                wN=(1/excitation-1/wL)*1E7
                wavenumbers.append(wN)

        self.waveNumbers=np.asarray(wavenumbers)

        time_Strings=dataDimensions.find("SeriesAxisArray").text.split(" ")
        times=[]
        for tS in time_Strings:
            if len(tS) > 0:
                t = float(tS)
                times.append(t)
        self.times=np.asarray(times)
        dataSize=int(root.find("./ScannedFrameParameters/DataBlockSizeBytes").text)


        with open(filePath,"rb") as file:
            file.seek(-dataSize,2) #seek from end
            buffer=file.read(dataSize)
            dtype=np.dtype(np.float32)
            data=np.frombuffer(buffer,dtype=dtype)
            spectra=data.reshape((len(self.times),channelSize,))
            self.spectra=np.transpose(spectra)


    def filteredSpectra(self, filter):
        if "SDEV" in  filter:
            sdev=np.std(self.spectra,axis=0)
            sortedIndices=np.argsort(sdev)
            half=math.floor(len(sortedIndices)/2)
            if filter == "BOTTOMSDEV":
                top=sortedIndices[:half]
                topSpectra=np.take(self.spectra,top,axis=1)
            else:
                top = sortedIndices[half:]
                topSpectra = np.take(self.spectra, top, axis=1)
            return topSpectra
        else:
            return self.spectra


    def interpolatedSpectra(self, start:float, end:float, points:int, filter="none",removeBackground=False,derivative:int =- 1 ,windowSize:int=41,augment:Augmentation=None):
        newX = np.linspace(start=start, stop=end, num=points)

        if filter == "none":
            spectra=self.spectra
        else:
            spectra=self.filteredSpectra(filter)

        interpolator = interpolate.interp1d(self.waveNumbers, spectra, axis=0)
        newY = interpolator(newX)

        if augment is not None:
            augmented=self.augmentData(inMatrix=spectra, start=start,end=end,points=points,augment=augment)
            newY=np.append(newY,augmented,axis=1)

        if removeBackground == True:
            newY=self.backgroundRemovedSpectra(inMatrix=newY)


        if derivative >= 0:
            newY = self.calculateDerivative(inMatrix=newY,
                                            xValues=newX,
                                            derivative=derivative,
                                            windowSize=windowSize)


        classes = np.full((newY.shape[1], 1), self.type)
        return newY, classes

    def augmentData(self,inMatrix,start:float, end:float, points:int,augment:Augmentation=None):
        augmented=[]
        newX = np.linspace(start=start, stop=end, num=points)
        for idx in range(augment.fold):
            deltaX=random.uniform(-augment.deltaX,augment.deltaX)
            shiftedWN=self.waveNumbers+deltaX
            interpolator = interpolate.interp1d(shiftedWN, inMatrix, axis=0)
            newY=interpolator(newX)
            multiplierNoise=np.random.randn(points)*augment.deltaYMultiplierNoise+1
            newY*=multiplierNoise[:,None]
            noise=np.random.randn(points)*augment.deltaYNoise
            proportionalNoise=newY*noise[:,None]
            newY+=proportionalNoise
            augmented.append(newY)

        if augment.combine is True:
            originalAugmented=augmented.copy()
            augmented.clear()
            for series in originalAugmented:
                currentSeries=np.copy(series)
                currentMin=np.amin(currentSeries,axis=0)
                # currentMax=np.amax(currentSeries,axis=0)

                randomSeries=np.copy(random.choice(originalAugmented))
                randomSeries=np.transpose(randomSeries)
                np.random.shuffle(randomSeries)
                randomSeries=np.transpose(randomSeries)
                randomWeight=np.random.randn(randomSeries.shape[1])+1
                weightedRandomSeries=randomSeries * randomWeight[None,:]
                combinedSeries=currentSeries+weightedRandomSeries
                newMin=np.amin(combinedSeries,axis=0)
                combinedSeries -= newMin[None,:]
                combinedSeries += currentMin[None,:]
                augmented.append(combinedSeries)


        return np.concatenate(augmented,axis=1)


    def calculateDerivative(self,inMatrix,xValues,derivative,windowSize,polyDegree=3):
        delta = (xValues[-1] - xValues[0]) / len(xValues)
        derMatrix = savgol_filter(inMatrix,
                                  window_length=windowSize,
                                  polyorder=polyDegree,
                                  deriv=derivative,
                                  delta=delta,
                                  mode="nearest",
                                  axis=0)

        return derMatrix


    #Eilers, P. & Boelens, H., 2005. Baseline correction with asymmetric least squares smoothing
    def backgroundRemovedSpectra(self,inMatrix=None,lambdaVal=5000, pVal=0.1,):
        if inMatrix is None:
            inMatrix=self.spectra

        outMatrix=[]
        M_D=self.makeMD(np.transpose(inMatrix)[0].shape)
        for spectrum in np.transpose(inMatrix):
            result=self.removeBackground(spectrum,lambdaVal=lambdaVal,pVal=pVal,M_D=M_D)
            outMatrix.append(result)
        outResult=np.asarray(outMatrix)
        return np.transpose(outResult)

    def makeMD(self,shape):
        M_D = np.zeros((shape[0] - 1, shape[0]))
        for p in range(M_D.shape[0]):  # this should work more elegantly without loops!
            for q in range(M_D.shape[1]):
                if p == q:
                    M_D[p, q] = -1
                elif p == q - 1:
                    M_D[p, q] = 1
                else:
                    M_D[p, q] = 0
        return M_D

    def removeBackground(self,inVector,lambdaVal=5000, pVal=0.1,M_D=None):

        inVectorDuplicate=np.copy(inVector)

        if M_D is None:
           M_D=self.makeMD(inVector.shape)


        W_One=np.ones_like(inVector)
        M_Diagonal=np.diag(W_One)
        stopVal=3
        M_B=np.zeros_like(inVector)
        for _ in range(0,stopVal):
            L=(M_Diagonal + lambdaVal * np.transpose(M_D) @ M_D)
            weightedIn=W_One * inVectorDuplicate
            M_B=np.linalg.solve(L,weightedIn)

            W_One = np.where(inVectorDuplicate < M_B, pVal, 1-pVal)

        result=inVectorDuplicate-M_B

        return result



    # def nnMatrixComponents(self, startWN, endWN, numComponents):
    #     interpolatedSpectra = self.interpolatedSpectra(start=startWN, end=endWN, points=round(endWN - startWN))[0]
    #     nmf = decomposition.NMF(numComponents)
    #     c = nmf.fit_transform(np.transpose(interpolatedSpectra))
    #     classes = np.full((c.shape[0], 1), self.type)
    #     return np.transpose(c), classes

def sortRamanSampleList(list):
    return sorted(list, key=lambda l: (l.prettyName().split("_")[0], int(l.prettyName().split("_")[1])))

def splitData(samples, percentValidation):
    shuffledSpectra = random.sample(samples, len(samples))
    idx = math.floor(len(shuffledSpectra) * (1 - percentValidation))
    trainingSamples = shuffledSpectra[:idx]
    validationSamples = shuffledSpectra[idx:]

    return trainingSamples, validationSamples


def makeSVMInputData(trainingSpectraList, validationSpectraList, normalize=True):
    trainingSpectra = np.transpose(np.concatenate([t[0] for t in trainingSpectraList], axis=1))
    trainingClasses = np.concatenate([t[1] for t in trainingSpectraList]).ravel()
    validationSpectra = np.transpose(np.concatenate([v[0] for v in validationSpectraList], axis=1))
    validationClasses = np.concatenate([v[1] for v in validationSpectraList]).ravel()

    if normalize == True:
        preprocessing.minmax_scale(trainingSpectra, axis=1, copy=False)
        preprocessing.minmax_scale(validationSpectra, axis=1, copy=False)

    return trainingSpectra, trainingClasses, validationSpectra, validationClasses

def makeMultiDimensionalData(trainingSpectraList, validationSpectraList, normalize=True, oneHot=False):

    def normalizeSpectra(inMatrix):
        if normalize == True:
            return preprocessing.minmax_scale(inMatrix,axis=0)
        else:
            return inMatrix


    trainingSpectra = np.transpose(np.dstack([normalizeSpectra(t[0]) for t in trainingSpectraList]))
    trainingClasses = np.concatenate([t[1][0] for t in trainingSpectraList]).ravel()
    validationSpectra = np.transpose(np.dstack([normalizeSpectra(v[0]) for v in validationSpectraList]))
    validationClasses = np.concatenate([v[1][0] for v in validationSpectraList]).ravel()


    if oneHot == True:
        oneHotTrainingClasses = preprocessing.OneHotEncoder(sparse=False).fit_transform(trainingClasses.reshape(len(trainingClasses), 1))
        oneHotValidationClasses = preprocessing.OneHotEncoder(sparse=False).fit_transform(validationClasses.reshape(len(validationClasses), 1))
        return trainingSpectra, oneHotTrainingClasses, validationSpectra, oneHotValidationClasses
    else:
        return trainingSpectra, trainingClasses, validationSpectra, validationClasses


def importData(path, classes=["kleb", "spore", "PS", "yeast"]):
    files = os.listdir(path)
    spectra = []
    for (file) in files:
        p = path + "/" + file
        extension = os.path.splitext(p)[1]
        filename = os.path.split(p)[1]
        shouldImport = False

        for c in classes:
            if c in filename:
                shouldImport = True
                break

        supportedFiletype=False
        for f in RamanSample.filetypes:
            if extension == f:
                supportedFiletype=True
                break

        if os.path.isfile(p) and supportedFiletype is True and shouldImport is True and not filename.startswith("."):
            s = RamanSample(p)
            spectra.append(s)

    return spectra

def prepareRandomData(spectraList, validationFraction=0.25, oneHot=False, start=201, end=1600, points=1000, filter="none"):
    trainingSamples, validationSamples = splitData(spectraList, validationFraction)
    trainingSpectraList = list(map(lambda x: x.interpolatedSpectra(start, end, points, filter=filter), trainingSamples))
    validationSpectraList = list(map(lambda x: x.interpolatedSpectra(start, end, points, filter=filter), validationSamples))
    trainingData, trainingClasses, validationData, validationClasses = makeSVMInputData(trainingSpectraList, validationSpectraList)

    if oneHot == True:
        oneHotTrainingClasses = preprocessing.OneHotEncoder(sparse=False).fit_transform(trainingClasses.reshape(len(trainingClasses), 1))
        oneHotValidationClasses = preprocessing.OneHotEncoder(sparse=False).fit_transform(validationClasses.reshape(len(validationClasses), 1))
        return trainingData,oneHotTrainingClasses,validationData,oneHotValidationClasses
    else:
        return trainingData, trainingClasses, validationData, validationClasses

# spectra=importData("/Users/morten/Documents/Raman/20180211_SKYP")
# augment=Augmentation(fold=1,deltaX=3,deltaYNoise=0.00,deltaYMultiplierNoise=0,combine=True)
# inter=spectra[0].interpolatedSpectra(start=204,end=1600,points=1000,augment=augment)
# print(inter)