import TimeSliceImport
from sklearn import (manifold, random_projection,decomposition)
import numpy as np

path = "/Users/morten/Desktop/SKYP_KYP_SYP/"

start = 201
end = 1600
points = 1000

spectra=TimeSliceImport.importData(path)

trainingSamples, validationSamples = TimeSliceImport.splitData(spectra, 0.25)

trainingSpectraList = list(map(lambda x: x.interpolatedSpectra(start, end, points), trainingSamples))

validationSpectraList = list(map(lambda x: x.interpolatedSpectra(start, end, points), validationSamples))

input = TimeSliceImport.makeSVMInputData(trainingSpectraList, validationSpectraList)

rp = random_projection.SparseRandomProjection(n_components=3)
random3D = rp.fit_transform(input[0])
random3DOut = np.column_stack([input[1], random3D])
np.savetxt(path + "/out/random3d.txt", X=random3DOut)
print(rp.get_params())

x= np.hsplit(random3D,3)


svd=decomposition.TruncatedSVD(n_components=3)
PCA=svd.fit_transform(input[0])
PCAOut=np.column_stack([input[1],PCA])
np.savetxt(path+"/out/PCA3d.txt",X=PCAOut)
print(svd.get_params())


iso=manifold.Isomap(n_neighbors=30,n_components=3)
isoTrans=iso.fit_transform(input[0])
isoOut=np.column_stack([input[1],isoTrans])
np.savetxt(path+"/out/iso30.txt",X=isoOut)
print(iso.reconstruction_error())

iso=manifold.Isomap(n_neighbors=200,n_components=3)
isoTrans=iso.fit_transform(input[0])
isoOut=np.column_stack([input[1],isoTrans])
np.savetxt(path+"/out/iso200.txt",X=isoOut)
print(iso.reconstruction_error())

mds=manifold.MDS(n_components=3)
mdsTrans=mds.fit_transform(input[0])
mdsOut=np.column_stack([input[1],mdsTrans])
np.savetxt(path+"/out/MDS3D.txt",X=mdsOut)


PCA100 = decomposition.PCA(n_components=150)
PCA100Trans = PCA100.fit_transform(input[0])
tsnb = manifold.TSNE(n_components=3, verbose=10)
TSNBTrans = tsnb.fit_transform(PCA100Trans)

TSNOut = np.column_stack([input[1], TSNBTrans])
np.savetxt(path + "/out/tsn.txt", X=TSNOut)


TSNBTrans = tsnb.fit_transform(input[0])
TSNOut = np.column_stack([input[1], TSNBTrans])
np.savetxt(path + "/out/tsn.txt", X=TSNOut)


kernelPCA=decomposition.KernelPCA(n_components=3,kernel="rbf",gamma=0.01)
kPCATrans=kernelPCA.fit_transform(input[0])
kpcaOut = np.column_stack([input[1], kPCATrans])
np.savetxt(path + "/out/kPCA.txt", X=kpcaOut)