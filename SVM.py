import TimeSliceImport
import os
from sklearn import (manifold,random_projection,decomposition,svm)
import numpy as np


def classifySVM(trainingSpectra, trainingClasses, validationSpectra, validationClasses):
    nuSVM = svm.NuSVC(nu=0.05, gamma=0.001, probability=True)
    nuSVM.fit(trainingSpectra, trainingClasses)
    prediction = nuSVM.predict(trainingSpectra)
    correct = 0

    for (pred, truth) in zip(prediction.tolist(), trainingClasses.tolist()):
        if pred == truth:
            correct += 1

    print("Training Data Correct {} ".format(correct / prediction.shape[0]))

    prediction = nuSVM.predict(validationSpectra)
    correctVal = 0

    for (pred, truth) in zip(prediction.tolist(), validationClasses.tolist()):
        if pred == truth:
            correctVal += 1

    print("Validation Data Correct {} ".format(correctVal / prediction.shape[0]))
    return correct, correctVal


path = "/Users/morten/Desktop/SKYP_KYP_SYP/"

start = 201
end = 1600
points = 1000

spectra=TimeSliceImport.importData(path, ["yeast","PS"])

trainingSamples, validationSamples = TimeSliceImport.splitData(spectra, 0.25)


trainingSpectraList = list(map(lambda x: x.interpolatedSpectra(start, end, points), trainingSamples))

validationSpectraList = list(map(lambda x: x.interpolatedSpectra(start, end, points), validationSamples))

input = TimeSliceImport.makeSVMInputData(trainingSpectraList, validationSpectraList)



# param_grid = {'nu': [0.001, 0.01, 0.05, 0.1, 0.2],
#               'gamma': [0.0001, 0.0005, 0.001, 0.005, 0.01, 0.1], }
# clf = GridSearchCV(svm.NuSVC(), param_grid)
# nnInput=makeSVMInputData(trainingNNComponentsList,validationNNComponentsList)
# #clf=clf.fit(input[0],input[1])
# #print(clf.best_estimator_)
#
# classifySVM(input[0],input[1],input[2],input[3])