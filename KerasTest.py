import keras
from keras.models import Sequential
from keras.layers import Dense, Dropout, Activation, LSTM
from keras.optimizers import SGD
import TimeSliceImport
import numpy as np
import StatefulLSTM



path="/Users/morten/Desktop/SKYP_KYP_SYP/"
start=201
end=1600
points=1000
types=["PS", "yeast","spore","kleb"]
spectra=TimeSliceImport.importData(path,types)
classes=list(map(lambda x:TimeSliceImport.RamanSample.mappings[x],types))
sortedTypesTuples=sorted(zip(types,classes),key=lambda x:x[1])
sortedTypes=[x[0] for x in sortedTypesTuples]
numClasses=len(types)


def makeModel(lossFunction="categorical_crossentropy", dropOut=0.0, recurrentDropout=0.0, nodes=32):
    # expected input data shape: (batch_size, timesteps, data_dim)
    model = Sequential()
    model.add(LSTM(nodes, return_sequences=True,
                   batch_input_shape=(1, 1, points), dropout=dropOut,
                   stateful=True))  # returns a sequence of vectors of dimension 32
    model.add(LSTM(nodes, return_sequences=True, dropout=dropOut,
                   stateful=True))  # returns a sequence of vectors of dimension 32
    model.add(LSTM(nodes, dropout=dropOut, stateful=True))  # return a single vector of dimension 32
    model.add(Dense(numClasses, activation='softmax'))
    model.compile(loss=lossFunction, optimizer='rmsprop', metrics=['accuracy'])
    return model


trainingSamples, validationSamples = TimeSliceImport.splitData(spectra, 0.25)
model = makeModel()

StatefulLSTM.trainModel(model,trainingSamples,numClasses, epochs=3)
StatefulLSTM.testModel(model, validationSamples, numClasses=numClasses)
StatefulLSTM.classifyData(model,validationSamples)










