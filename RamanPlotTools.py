import matplotlib.pyplot as plt
import itertools
import numpy as np


def plotConfusionMatrix(mtrx, labels, title="", normalize=False):
    plt.figure()

    if normalize:
        mtrx = mtrx.astype('float') / mtrx.sum(axis=1)[:, np.newaxis]

    plt.imshow(mtrx, interpolation='nearest', cmap=plt.cm.Blues)
    plt.colorbar()
    plt.tight_layout()

    tick_marks = np.arange(len(labels))
    plt.xticks(tick_marks, labels, rotation=45)
    plt.yticks(tick_marks, labels)
    plt.ylabel('True label')
    plt.xlabel('Predicted label')
    plt.title(title)
    fmt = '.2f' if normalize else 'd'
    thresh = mtrx.max() / 2.
    for i, j in itertools.product(range(mtrx.shape[0]), range(mtrx.shape[1])):
        plt.text(j, i, format(mtrx[i, j], fmt), horizontalalignment="center",
                 color="white" if mtrx[i, j] > thresh else "black")
    plt.show()


def plotResult(result, truth, truthLabels):
    fig, axes = plt.subplots()
    axes.plot(truth, color="red", label="input")
    axes.plot(result, color="blue", marker="x", linestyle="-", linewidth=0.2, label="classification result")
    plt.xlabel("trace #")
    tick_marks = sorted(np.unique(truth))
    plt.yticks(tick_marks, truthLabels)
    plt.legend()
    plt.show()
